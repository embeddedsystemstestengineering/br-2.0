def init_powersupply():
	import serial
	global ser
	ser = serial.Serial()
	ser.baudrate = 9600
	ser.port = '/dev/ttyUSB1'
	ser.timeout = 1
	ser.open()
	#open = ser.is_open
	ser.flush

#Enables the remote mode
def enable_remote_mode():
	global ser
	cmd=[0xAA,0,0x20,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0xcb]
	ser.write(bytes(cmd))
	#resp = ser.read(26)

#Turns on the power supply display	
def setting_output_state():
	global ser
	cmd=[0xAA,0,0x21,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0xcc]
	ser.write(bytes(cmd))
	#resp = ser.read(26)

#sets the power supply max current to 0.5 A	
def setting_output_current():
	global ser
	cmd=[0xAA,0,0x24,0xf4,0x01,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0xc3]
	ser.write(bytes(cmd))
	#resp = ser.read(26)

#Sets the voltage to 50V	
def setting_voltage():
	global ser
	cmd=[0xAA,0,0x23,0x50,0xC3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0xe0]
	ser.write(bytes(cmd))

def balancing_voltage():
	global ser
	cmd=[0xAA,0,0x23,0xC8,0xAF,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0x44]
	ser.write(bytes(cmd))

#Sets the voltage to 50V for Nominal voltage test	
def setting_nomvoltage():
	global ser
	cmd=[0xAA,0,0x23,0x50,0xC3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0xe0]
	ser.write(bytes(cmd))
	#resp = ser.read(26)

#Sets the voltage to 58V for Over voltage test	
def setting_overvoltage():
	global ser
	cmd=[0xAA,0,0x23,0x90,0xe2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0x3f]
	ser.write(bytes(cmd))
	#resp = ser.read(26)

#Sets the voltage to 30V for Under voltage test	
def setting_undervoltage():
	global ser
	cmd=[0xAA,0,0x23,0x30,0x75,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0x72]
	ser.write(bytes(cmd))
	#resp = ser.read(26)

#Sets the voltage to 0V
def shutting_down():
	global ser
	cmd=[0xAA,0,0x20,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0xcb]
	ser.write(bytes(cmd))
	
#Closing the serial	
def serial_close():
	global ser
	ser.close()
#input("Hit [enter] to exit?")