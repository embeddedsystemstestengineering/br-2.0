import ctypes
import serial
from serial import *
import signal
import sys
import time
import xlsxwriter
import os

from colorama import Fore, init, Back, Style
from datetime import date, datetime
from pprint import pprint

from pl455_config import *
from BK1787B import *
from Cell_balancing import *

# colorama
init(autoreset = True)

filepath=""
PASS_DATA = {}
TEST_RESULT = "ALL_GOOD"
TEST_NAME = "name"
NUM_CELLS = 14
NUM_THERM = 8
EXPECTED_VALUE_CELL = 3.57
LSL_CELL = 3.51
USL_CELL = 3.66
EXPECTED_VALUE_THERM = 1.495 
LSL_THERM = 1.49
USL_THERM = 1.51
EXPECTED_CELL_AFTERBALANCING_VOLTAGE = 1.24
NUM_CELL_BLOCKS = 14
NUM_TEMP_SENSORS = 8
OUTPUT = "PASS"
CELL_BALANCING_OUTCOME = []

# Creating Xlsx worksheet Test Report
#Workbook Title
barcode_scan = input (Fore.CYAN + "Please scan or enter the barcode digits :")
wait_time = input (Fore.CYAN + "Please close the fixture lid and then hit ENTER..")
date_time = str(datetime.now().replace(microsecond=0)).replace(" ", "_").replace(":", "-") #Title
workbook = xlsxwriter.Workbook('/home/pi/Documents/BMB_results/'+ barcode_scan +'_' + date_time + '.xlsx')

#Worksheet Titles
worksheet1 = workbook.add_worksheet('Nominal_Voltage_Report')
worksheet2 = workbook.add_worksheet('Over_Voltage_Report')
worksheet3 = workbook.add_worksheet('Under_Voltage_Report')
worksheet4 = workbook.add_worksheet('Cell_Balancing_Report')

worksheets = [worksheet1, worksheet2, worksheet3]

#Adding title to various rows in worksheet
for worksheet in worksheets :

	bold = workbook.add_format({'bold': True})
	worksheet.write('A1', 'Test_Title : BMB PCBA Test', bold)
	worksheet.write('A2', 'Barcode_Scan :', bold)
	worksheet.write('B2', barcode_scan, bold) # printing actual value from barcode scan
	worksheet.write('A3', 'Test_type', bold)
	worksheet.write('B3', 'Signal_Name', bold)
	worksheet.write('C3', 'Expected_Value', bold)
	worksheet.write('D3', 'LSL', bold)
	worksheet.write('E3', 'Cell_Measured', bold)
	worksheet.write('F3', 'USL', bold)
	worksheet.write('G3', 'Status', bold)
	
	row_cell = 4
	row_therm = 18
	colB = 1
	colC = 2
	colD = 3
	colF = 5

	for cell in range(1, NUM_CELLS + 1):
		worksheet.write(row_cell, colB,"Cell " + str(cell))
		worksheet.write(row_cell, colC, EXPECTED_VALUE_CELL)
		worksheet.write(row_cell, colD, LSL_CELL)
		worksheet.write(row_cell, colF, USL_CELL)
		row_cell +=1

	for therm in range(1, NUM_THERM + 1):
		worksheet.write(row_therm, colB,"Ts " + str(therm))
		worksheet.write(row_therm, colC, EXPECTED_VALUE_THERM)
		worksheet.write(row_therm, colD, LSL_THERM)
		worksheet.write(row_therm, colF, USL_THERM)
		row_therm +=1

bold = workbook.add_format({'bold': True})
worksheet4.write('A1', 'Test_Title : BMB PCBA Test', bold)
worksheet4.write('A2', 'Barcode_Scan :', bold)
worksheet4.write('B2', barcode_scan, bold) # printing actual value from barcode scan
worksheet4.write('A3', 'Test_type', bold)
worksheet4.write('B3', 'Signal_Name', bold)
#worksheet4.write('C3', 'Expected_Value', bold)
worksheet4.write('C3', 'Cell balancing result', bold)
#worksheet4.write('E3', 'Status', bold)
worksheet4.write('A4', 'Cell_Balancing_Test', bold)

row_balance = 4
colB = 1
colC = 2
colD = 3
colF = 5

for balance_cell in range(1, NUM_CELLS + 1):
	worksheet4.write(row_balance, colB,"Cell " + str(balance_cell))
	#worksheet4.write(row_balance, colC, EXPECTED_CELL_AFTERBALANCING_VOLTAGE)
	row_balance +=1
	   
def signal_handler(signal, frame):
	sys.exit(0)
	
#Initializing the serial port
def init_serial(serial_port=SERIAL_PORT, baud_rate=SERIAL_BAUD_RATE, timeout=SERIAL_TIMEOUT, debug=False):

	if debug:
		print(Fore.YELLOW + "Connecting to device ... ")
		print(Fore.YELLOW + '\t' + "serial_port = " + str(serial_port))#Information imported from the config file
		print(Fore.YELLOW + '\t' + "baud_rate = " + str(baud_rate))
		print(Fore.YELLOW + '\t' + "timeout = " + str(timeout))
		print()

	try:
		ser = serial.Serial(serial_port, int(baud_rate), timeout=int(timeout))
	except serial.serialutil.SerialException:
		print (Fore.RED + "Error: Could not connect to device on " + str(serial_port) + " at " + str(baud_rate))
		exit(0)

	return ser

#Sending data
def send_byte_array(serial_device, data, debug=False):

	try:
		bytes_data = bytes(data)
	except:
		print ("The given array is not castable to bytes")
		exit(0)

	if debug:
		print (Fore.YELLOW + "Sending: " + str(data))

	serial_device.write(bytes_data)

#Receiving data
def recv_data(serial_device, timeout=SERIAL_TIMEOUT, debug=False):

	start_time = datetime.now()
	data = None

	while (data == None or data == b'') and ((datetime.now() - start_time).total_seconds() < timeout):
		data = serial_device.readline()

	if debug:
		print (Fore.GREEN + "Received: " + str(data))

	return data

#Sending sequence to device
def send_seqeunce_to_device(serial_device, seqeunce, timeout=SERIAL_TIMEOUT, debug=False):

	while True:
		for msg in seqeunce:
			send_byte_array(serial_device, msg, debug=debug)
			data = recv_data(serial_device, timeout, debug=debug)
			time.sleep(timeout/10)

		if len(data) > 0:
			# print('received data')
			return data

		time.sleep(timeout/10) #time.sleep(0.01) in seconds

		return

#Processing the data
def process_pl455_data(data_bytes, NUM_CELL_BLOCKS, NUM_TEMP_SENSORS, debug=False):

	if debug:
		print("Input data: ")
		print(data_bytes)

	data_int 	= int.from_bytes(data_bytes, byteorder='big')
	data_hex 	= hex(data_int)

	length 		= data_hex[0:4]
	voltages 	= []
	temps 		= []
	remaining 	= []

	data_hex 	= data_hex[4:]
	counter 	= 0

	while len(data_hex) > 0:

		tmp = data_hex[0:4]
		data_hex = data_hex[4:]

		tmp_int = int(tmp, 16)
		tmp_uint = ctypes.c_uint16(tmp_int).value

		if counter < NUM_CELL_BLOCKS:
			tmp_uint *= 50000
			tmp_uint = tmp_uint >> 16
			tmp_uint /= 10000
			voltages.append(tmp_uint)
			#time.sleep(5)
		elif counter >= NUM_CELL_BLOCKS and counter < (NUM_CELL_BLOCKS+NUM_TEMP_SENSORS):
			tmp_uint *= 5000
			tmp_uint = tmp_uint >> 16
			tmp_uint /= 1000
			temps.append(tmp_uint)
			#time.sleep(5)
		else:
			remaining.append(tmp_uint)

		counter += 1

	if debug:
		pprint(length)
		pprint(voltages)
		pprint(temps)
		pprint(remaining)

	return voltages, temps
	
#Defining the range function to used it later to check if cell and thermistor voltages are in range
def is_in_range(val, low, high):
	if val >= low and val <= high:
		return True
	return False

#Checking the data
def check_pl455_data(voltages, temps, volt_crit, temp_crit, worksheet):
	global OUTPUT
	
#Checking Cell voltages
	if len(voltages) != NUM_CELL_BLOCKS:
		print(Fore.RED + "\nError: Number of cells is less than " + str(NUM_CELL_BLOCKS))
		OUTPUT = "FAIL"
		print("\nNumber of Cells is " + str(len(voltages)))
		return

	passing = True

	print("\nVoltage [V]")
	counter = 0
	row_v = 4
	col_v = 4
	result_col = 6
	global TEST_NAME
	#global TEST_RESULT
	TEST_RESULT = "ALL_GOOD"
	for v in voltages: #(looping through each cell voltage one at a time)
		counter += 1
		print (counter, end=' ')
		print (str(v).ljust(8), end='\t')
		if is_in_range(v, volt_crit[0], volt_crit[1]):
			if TEST_NAME == 'Nominal_Voltage_Test':
				print (Fore.GREEN + "PASS")				
				worksheet.write(row_v, result_col, "PASS")
				#TEST_RESULT = "ALL_GOOD"
			else :
				print (Fore.RED + "FAIL")				
				worksheet.write(row_v, result_col, "FAIL")
				TEST_RESULT = "AT_LEAST_ONE_BAD"
				
		else :
			if TEST_NAME == 'Nominal_Voltage_Test':
				print (Fore.RED + "FAIL")								
				worksheet.write(row_v, result_col, "FAIL")
				TEST_RESULT = "AT_LEAST_ONE_BAD"
			if TEST_NAME == 'Over_Voltage_Test' or TEST_NAME == 'Under_Voltage_Test':
				print (Fore.GREEN + "PASS")
				worksheet.write(row_v, result_col, "PASS")
				#TEST_RESULT = "ALL_GOOD"
		worksheet.write(row_v, col_v, v)
		row_v += 1
		
	if TEST_RESULT == "ALL_GOOD" :		
		print(Fore.GREEN + TEST_NAME + " Passed")
	if TEST_RESULT == "AT_LEAST_ONE_BAD" :
		OUTPUT = "FAIL"
		print(Fore.RED + TEST_NAME + " Failed")

#Checking Thermistor voltages
	print("\nTemps [C]")
	counter = 0
	row_t = 18
	col_t = 4
	TEST_RESULT = "ALL_GOOD"
	for t in temps:
		counter += 1
		print (counter, end=' ')
		print (str(t).ljust(8), end='\t')
		if is_in_range(t, temp_crit[0], temp_crit[1]):
			print (Fore.GREEN + "PASS") 
			worksheet.write(row_t, result_col, "PASS")
			#TEST_RESULT = "ALL_GOOD"
		else :
			print (Fore.RED + "FAIL")
			worksheet.write(row_t, result_col, "FAIL")
			TEST_RESULT = "AT_LEAST_ONE_BAD"
		
		worksheet.write(row_t, col_t, t)
		row_t += 1
		
	if TEST_RESULT == "ALL_GOOD" :		
		print(Fore.GREEN + "Thermistor Voltage Test Passed")		
	if TEST_RESULT == "AT_LEAST_ONE_BAD" :
		OUTPUT = "FAIL"
		print(Fore.RED + "Thermistor Voltage Test Failed")	

	print()

	return passing

def cell_balancing_datacheck(volts, critical_value, cell_num):#, worksheet):

	global OUTPUT 
	global TEST_RESULT
	global CELL_BALANCING_OUTCOME
		
#Checking Balanced Cell voltages
	if len(voltages) != NUM_CELL_BLOCKS:
		print(Fore.RED + "\nError: Number of cells is less than " + str(NUM_CELL_BLOCKS))
		#OUTPUT = "FAIL"
		print("\nNumber of Cells is " + str(len(voltages)))
		return

	passing = True

	print("\nVoltage [V]")
	counter = 0
	#status_col = 4
	#global TEST_NAME
	
	TEST_RESULT = "ALL_GOOD"
	for index, v in enumerate(voltages): #(looping through each cell voltage one at a time)
		counter += 1
		print (counter, end=' ')
		print (str(v).ljust(8), end='\t')		
		if is_in_range(v, critical_value[0], critical_value[1]):
			if(index==cell_num):
				print (Fore.GREEN + "PASS: Cell voltage dropped as expected")
				#worksheet.write(balance_row, status_col, "Cell Voltage dropped as per expectation")				
			else:
				print (Fore.RED + "FAIL: This is not the correct cell number")
				#worksheet.write(balance_row, status_col, "This is not the correct cell")
				OUTPUT ="FAIL"
				CELL_BALANCING_OUTCOME[cell] = False
				TEST_RESULT == "AT_LEAST_ONE_BAD"
		else:
			if(index==cell_num):
				print (Fore.RED + "FAIL: Cell voltage did not drop")
				#worksheet.write(balance_row, status_col, "Cell Voltage did not drop")
				OUTPUT = "FAIL"
				CELL_BALANCING_OUTCOME[cell] = False
				TEST_RESULT == "AT_LEAST_ONE_BAD"
			else:
				print (Fore.GREEN + "PASS: Cell voltage dropped")
				#worksheet.write(balance_row, status_col, "Cell Voltage dropped")
		
		#worksheet.write(balance_row, balance_col, v)
		#balance_row += 1
		
	if TEST_RESULT == "ALL_GOOD" :		
		print(Fore.GREEN + "Cell Balancing Test Passed")		
	if TEST_RESULT == "AT_LEAST_ONE_BAD" :
		OUTPUT = "FAIL"
		print(Fore.RED + "Cell Balancing Test Failed")
		passing = False	

	print ()
	return passing

def voltage_test(input_voltage, worksheet):
         
	PRINT_DEBUG = False
	NUM_TEST = 3
	global ser
	print(Fore.YELLOW + TEST_NAME)
	
	#test_name = input(Fore.YELLOW + "Set the power supply to " + str(input_voltage) + "V 1A and then press ENTER key...")
	
	print(Fore.BLUE + "Setting powersupply as per testcase")
	if TEST_NAME == 'Nominal_Voltage_Test':
		setting_nomvoltage()
		print (Fore.YELLOW + "Power Supply Voltage set to " + str(nominal_voltage) + "V") 
	
	elif TEST_NAME == 'Over_Voltage_Test':
		setting_overvoltage()
		print (Fore.YELLOW + "Power Supply Voltage set to " + str(over_voltage) + "V")
		
	elif TEST_NAME == 'Under_Voltage_Test':
		setting_undervoltage()
		print (Fore.YELLOW + "Power Supply Voltage set to " + str(under_voltage) + "V")
	
	print(Fore.BLUE + "0 Initializing pl455 on serial")
	pl455 = init_serial(SERIAL_PORT, SERIAL_BAUD_RATE, SERIAL_TIMEOUT, debug=PRINT_DEBUG)

	print(Fore.BLUE + "1 Sending init sequence to pl455")
	send_seqeunce_to_device(pl455, INIT_SEQUENCE, debug=PRINT_DEBUG)

	for a in range(0, NUM_TEST):
		print(Fore.BLUE + "2 Requesting data from pl455")
		data = None
		delaycount = 0
		WAIT = 10
		while (data == None) and (delaycount < WAIT):
			data = send_seqeunce_to_device(pl455, READ_DATA_SEQUENCE, debug=PRINT_DEBUG)
			delaycount+=1
			time.sleep(1)  #sleep for a second before starting new loop iteration

		if(delaycount > WAIT and data == None):
			print(Fore.RED + "Did not receive data, exiting..")
			break # Break the for loop if it has timedout
		
		print(Fore.BLUE + "3 Processing received data")
		voltages, temps = process_pl455_data(data, NUM_CELL_BLOCKS, NUM_TEMP_SENSORS)

		print(Fore.BLUE + "4 Checking data against criteria")
		result = check_pl455_data(voltages, temps, VOLTAGE_ACCEPTANCE_CRITERIA, TEMP_ACCEPTANCE_CRITERIA, worksheet)
		
		
if __name__ == '__main__':

	signal.signal(signal.SIGINT, signal_handler)
	
	global ser	
	print(Fore.BLUE + "Initializing power supply")
	init_powersupply()
	enable_remote_mode()
	setting_output_state()
	setting_output_current()
	setting_voltage()
	PRINT_DEBUG = False
	NUM_TEST = 2
	#global CELL_BALANCING_OUTCOME
	#global balance_row
	#global balance_col
		
	nominal_voltage = 50
	TEST_NAME = 'Nominal_Voltage_Test'
	voltage_test(nominal_voltage,worksheet1)
	worksheet1.write('A4', 'Nominal_Voltage = ' + str(nominal_voltage) + 'V', bold)
	
	over_voltage = 58
	TEST_NAME = 'Over_Voltage_Test'
	voltage_test(over_voltage,worksheet2)
	worksheet2.write('A4', 'Over_Voltage = ' + str(over_voltage) + 'V', bold)
	
	under_voltage = 30
	TEST_NAME = 'Under_Voltage_Test'
	voltage_test(under_voltage,worksheet3)
	worksheet3.write('A4', 'Under_Voltage = ' + str(under_voltage) + 'V', bold)
	
	print(Fore.YELLOW + "Cell Balancing Test")
	balancing_voltage() # setting power supply voltage to 50V before cell balancing
	
	print(Fore.BLUE + "0 Initializing pl455 on serial")
	pl455 = init_serial(SERIAL_PORT, SERIAL_BAUD_RATE, SERIAL_TIMEOUT, debug=PRINT_DEBUG)
	
	#time.sleep(5)

	print(Fore.BLUE + "1 Sending init sequence to pl455")
	send_seqeunce_to_device(pl455, INIT_CELL_SEQUENCE, debug=PRINT_DEBUG)
	
	print(Fore.BLUE + "2 Sending clear balancing sequence to pl455")
	send_seqeunce_to_device(pl455, CLEAR_BALANCING, debug=PRINT_DEBUG)
	
	balance_row = 4
	balance_col = 2
		
	for cell in range (0, NUM_CELL_BLOCKS):
		global ser
		
		counter = 0
		PRINT_DEBUG = False
		CELL_BALANCING_OUTCOME.append(True)

		for x in range(0, NUM_TEST):

			print(Fore.BLUE + "3 Sending balancing sequence to pl455")
			send_seqeunce_to_device(pl455, CELL_BALANCING_SEQUENCE[cell], debug=PRINT_DEBUG)
		
			time.sleep(5)

			print(Fore.BLUE + "4 Requesting data from pl455")
			data = None
			attempt_count = 0
			attempt_limit = 10
			while (data == None) and (attempt_count < attempt_limit):
			# this function returns the data read by READ_DATA Command
				time.sleep(1)
				data = send_seqeunce_to_device(pl455, READ_DATA_SEQUENCE, debug=PRINT_DEBUG)
				attempt_count+=1
				time.sleep(1)  #sleep for a second before starting new loop iteration

			if(attempt_count > attempt_limit and data == None):
				print(Fore.RED + "Did not receive data, exiting..")
				break # Break the for loop if it has timedout
				
			time.sleep(5)
			print(Fore.BLUE + "5 Processing received data")
			voltages, temps = process_pl455_data(data, NUM_CELL_BLOCKS, 0)

			print(Fore.BLUE + "6 Checking data against criteria")
			CELL_BALANCING_OUTCOME[cell] = cell_balancing_datacheck(voltages, BALANCING_ACCEPTANCE_CRITERIA, cell)
			if (CELL_BALANCING_OUTCOME[cell] == True):
				worksheet4.write(balance_row + cell, balance_col,"Pass")
			elif (CELL_BALANCING_OUTCOME[cell] == False):
				worksheet4.write(balance_row + cell, balance_col,"Fail")
					 
			print(Fore.BLUE + "7 Sending clear balancing sequence to pl455")
			send_seqeunce_to_device(pl455, CLEAR_BALANCING, debug=PRINT_DEBUG)
		
		counter +=1
	
	if OUTPUT == "PASS" :
		print (Fore.GREEN + "All tests PASSED.")
	if OUTPUT == "FAIL" :
		print(Fore.RED + "One or more tests FAILED.")

	workbook.close()
	shutting_down()
	serial_close()
	clear_screen = input(Fore.BLUE + "Please press ENTER to clear screen..")
	os.system('clear')
