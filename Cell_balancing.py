import serial
global ser

CELL_BALANCING_SEQUENCE = [
	[[0x9A, 0X00, 0X00, 0X14, 0X00, 0X01, 0X9C, 0X3E]],
	[[0x9A, 0X00, 0X00, 0X14, 0X00, 0X02, 0XDC, 0X3F]],
	[[0x9A, 0X00, 0X00, 0X14, 0X00, 0X04, 0X5C, 0X3D]],
	[[0x9A, 0X00, 0X00, 0X14, 0X00, 0X08, 0X5C, 0X38]],
	[[0x9A, 0X00, 0X00, 0X14, 0X00, 0X10, 0X5C, 0X32]],
	[[0x9A, 0X00, 0X00, 0X14, 0X00, 0X20, 0X5C, 0X26]],
	[[0x9A, 0X00, 0X00, 0X14, 0X00, 0X40, 0X5C, 0X0E]],
	[[0x9A, 0X00, 0X00, 0X14, 0X00, 0X80, 0X5C, 0X5E]],
	[[0x9A, 0X00, 0X00, 0X14, 0X01, 0X00, 0X5C, 0X6E]],
	[[0x9A, 0X00, 0X00, 0X14, 0X02, 0X00, 0X5C, 0X9E]],
	[[0x9A, 0X00, 0X00, 0X14, 0X04, 0X00, 0X5F, 0X3E]],
	[[0x9A, 0X00, 0X00, 0X14, 0X08, 0X00, 0X5A, 0X3E]],
	[[0x9A, 0X00, 0X00, 0X14, 0X10, 0X00, 0X50, 0X3E]],
	[[0x9A, 0X00, 0X00, 0X14, 0X20, 0X00, 0X44, 0X3E]]
	]
	
CLEAR_BALANCING = [[0x9A, 0X00, 0X00, 0X14, 0X00, 0X00, 0X5D, 0XFE]]


INIT_CELL_SEQUENCE = [
	[0xF2, 0x6B, 0x80, 0x00, 0x22, 0xA4],					# 1 clear all fault summary flags
	[0xF2, 0x52, 0xFF, 0xC0, 0xD2, 0xC9], 					# 2 clear all fault summary flags
	[0xF1, 0x51, 0x38, 0x6D, 0xB1],							# 3 clear fault flags in the system status register
	[0xF1, 0x0E, 0x19, 0x94, 0x59],							# 4 set auto-address mode on all boards
	[0xF1, 0x0C, 0x08, 0x55, 0x35],							# 5 enter auto-address mode on all boards, the next write to this ID will be its address
	[0xF1, 0x0A, 0x00, 0x57, 0x53],							# 6 send address to each board
	[0xF2, 0x10, 0x10, 0x80, 0x3F, 0x1D],					# 7 set communications baud rate and enable all interfaces on all boards in stack
	[0xF2, 0x52, 0xFF, 0xC0, 0xD2, 0xC9],					# 8 clear all fault summary flags
	[0xF1, 0x51, 0x38, 0x6D, 0xB1],							# 9 clear fault flags in the system status register
	[0x91, 0x00, 0x3C, 0x00, 0x3D, 0xFC],					# 10 set 0 mux delay
	[0x91, 0x00, 0x3D, 0x00, 0x3C, 0x6C],					# 11 set 0 initial delay
	[0x91, 0x00, 0x3E, 0xCC, 0x3C, 0xC9],					# 12 set 99.92us ADC sampling period
	[0x91, 0x00, 0x07, 0x00, 0x2E, 0xCC],					# 13 set no oversampling period
	[0x91, 0x00, 0x51, 0x38, 0x10, 0xBE],					# 14 clear fault flags in the system status register
	[0x92, 0x00, 0x52, 0xFF, 0xC0, 0x59, 0xAC],				# 15 clear all fault summary flags
	[0x81, 0x00, 0x51, 0x00, 0x15, 0xAC],					# 16 0ms timeout	READ REG?
	[0x81, 0x00, 0x52, 0x01, 0xD4, 0x9C],					# 17 0ms timeout	READ REG?
	[0x91, 0x00, 0x0D, 0x0E, 0xA9, 0xA8],					# 18 set number of cells to 16
	[0x94, 0x00, 0x03, 0x3F, 0xFF, 0xFF, 0xC2, 0x6D, 0x8C]	# 19 select all cell, AUX channels 0 and 1, and internal digital die and internal analog die temperatures
	]

BALANCING_ACCEPTANCE_CRITERIA = [1, 1.4]

CELL_BALANCING_INIT = [0x9A, 0X00, 0X00, 0X13, 0X98, 0X54, 0X86] # will enable balancing for 60 minutes before automatically turning it off